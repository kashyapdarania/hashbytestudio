﻿using UnityEngine;

public class LaneManager : MonoBehaviour
{
    private const float LANE_WIDTH = 0.02f;
    private const float LANE_HEIGHT = 10f;

    [SerializeField]
    private Transform m_LanePrefab = default;

    [SerializeField]
    private Transform m_LaneParent = default;

    [Range(Constant.MIN_LANES, Constant.MAX_LANES)]
    public int lanesCount = 3;

    private void Awake()
    {
        CreateLanes();
    }

    private void CreateLanes()
    {
        // devide screen width into equal part as laneCounter and place lanes
        float l_Offset = Screen.width / lanesCount;

        Camera l_MainCamera = Camera.main;

        Transform l_Lane;
        Vector2 l_Position;
        for (int i = 1; i < lanesCount; i++)
        {
            l_Lane = Instantiate(m_LanePrefab);

            // set width and height
            l_Lane.localScale = new Vector3(LANE_WIDTH, LANE_HEIGHT, 1f);

            // we only intersted in x
            l_Position = l_MainCamera.ScreenToWorldPoint(new Vector3(l_Offset * i, 0f));
            l_Position.y = 0;
            l_Lane.position = l_Position;

            l_Lane.name = "Lane" + i;

            l_Lane.SetParent(m_LaneParent);
        }
    }
}
