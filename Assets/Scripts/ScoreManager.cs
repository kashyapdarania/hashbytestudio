﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager Instance;

    [SerializeField]
    private Text m_ScoreText = default;

    private int _score = 0;
    public int Score
    {
        get
        {
            return _score;
        }
        set
        {
            if (value == _score)
            {
                return;
            }
            _score = value;
            m_ScoreText.text = _score.ToString();
        }
    }

    private void Awake()
    {
        Instance = this;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(Constant.ENEMY_BODY_TAG))
        {
            Score += collision.GetComponentInParent<EnemyController>().ScoreValue;
        }
    }
}
