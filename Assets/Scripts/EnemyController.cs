﻿using UnityEngine;

public enum MovingDirection
{
    Clockwise,
    AntiClockwise
}

[RequireComponent(typeof(BoxCollider2D))]
public class EnemyController : MonoBehaviour
{
    private const float BOTTOM_OFFSET = 0.5f;

    [SerializeField]
    private float m_VerticleSpeed = 1f;

    [SerializeField]
    private Transform m_Body = default;

    public int ScoreValue = 1;

    public float Height
    {
        get
        {
            return GetComponentInChildren<SpriteRenderer>().bounds.size.y;
        }
    }

    private Vector2 m_Velocity;
    private float m_YBounds;
    public MovingDirection movingDirection;

    private BoxCollider2D m_BoxCollider;

    private Vector2 m_InitialOffset;
    private Vector2 m_InitialSize;

    private HorizontalMovingBehaviour m_MovingBehaviour;

    public int CurrentLane;
    public int TargetLane;

    private void Awake()
    {
        m_BoxCollider = GetComponent<BoxCollider2D>();
        m_InitialOffset = m_BoxCollider.offset;
        m_InitialSize = m_BoxCollider.size;

        m_MovingBehaviour = m_Body.GetComponent<HorizontalMovingBehaviour>();
    }

    private void Start()
    {
        m_Velocity = new Vector2(0f, -m_VerticleSpeed);
        m_YBounds = (Camera.main.orthographicSize + Height + BOTTOM_OFFSET) * -1;
    }

    private void Update()
    {
        transform.Translate(m_Velocity * Time.deltaTime);

        if (transform.position.y < m_YBounds)
        {
            gameObject.SetActive(false);
        }
    }

    private void OnDisable()
    {
        m_Body.transform.localPosition = Vector2.zero;

        m_BoxCollider.offset = m_InitialOffset;
        m_BoxCollider.size = m_InitialSize;
    }

    public void MoveTowards(int currentLane, int targetLane, float difference)
    {
        CurrentLane = currentLane;
        TargetLane = targetLane;

        if (currentLane > targetLane)
        {
            difference *= -1f;
            movingDirection = MovingDirection.AntiClockwise;
        }
        else
        {
            movingDirection = MovingDirection.Clockwise;
        }

        Vector2 l_Size = m_BoxCollider.size;
        Vector2 l_Offset = m_BoxCollider.offset;

        float l_AbsDifference = Mathf.Abs(difference);
        // change the collider size
        switch (movingDirection)
        {
            case MovingDirection.Clockwise:
                l_Size.x += l_AbsDifference;
                l_Offset.x += (l_AbsDifference / 2f);
                break;
            case MovingDirection.AntiClockwise:
                l_Size.x += l_AbsDifference;
                l_Offset.x -= l_AbsDifference / 2f;
                break;
        }
        m_BoxCollider.size = l_Size;
        m_BoxCollider.offset = l_Offset;

        m_MovingBehaviour.Initilize(Vector2.zero, new Vector2(difference, 0f));
        m_MovingBehaviour.enabled = true;
    }
}
