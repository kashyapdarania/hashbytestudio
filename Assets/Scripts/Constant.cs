﻿public static class Constant
{
    public const string ENEMY_TAG = "Enemy";
    public const string ENEMY_BODY_TAG = "EnemyBody";

    public const string SCORE_MESSAGE = "Score: {0}";
    public const string SURVIVED_TIME_MESSAGE = "Survived Time: {0}";

    public const int MIN_LANES = 3;
    public const int MAX_LANES = 5;

    public const int MOVING_ENEMY_GENERATION_START_TIME = 20;

    public static int GetRandomNumber()
    {
        return UnityEngine.Random.Range(0, 2);
    }

}
