﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public struct GameStartEvent
{
    public static GameStartEvent current;

    public static void Trigger()
    {
        EventManager.TriggerEvent<GameStartEvent>(current);
    }
}

public struct GameCompleteEvent
{
    public static GameCompleteEvent current;

    public static void Trigger()
    {
        EventManager.TriggerEvent<GameCompleteEvent>(current);
    }
}

public struct GenerateMovingEnemyEvent
{
    public static GenerateMovingEnemyEvent current;

    public static void Trigger()
    {
        EventManager.TriggerEvent<GenerateMovingEnemyEvent>(current);
    }
}

public class GameManager : MonoBehaviour, EventListener<GameStartEvent>,
                                          EventListener<GameCompleteEvent>
{
    [SerializeField]
    private GameObject m_GameStartPanel = default;

    [SerializeField]
    private GameCompletePanel m_GameCompletePanel = default;

    [SerializeField]
    private GameObject m_InGamePanel = default;

    [SerializeField]
    private GameObject m_InputPanel = default;

    [SerializeField]
    private LaneManager m_LaneManager = default;

    private int m_Timer = 0;
    private Coroutine m_TimerCoroutine;

    [SerializeField]
    private bool m_InputEnabled = false;

    public int LanesCount
    {
        get
        {
            return m_LaneManager.lanesCount;
        }
    }

    private void Awake()
    {
        Time.timeScale = 1;
    }

    private void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        m_GameStartPanel.SetActive(true);
        m_InputPanel.SetActive(false);
        m_GameCompletePanel.gameObject.SetActive(false);
        m_InGamePanel.SetActive(false);
    }

    private void OnEnable()
    {
        this.EventStartListening<GameStartEvent>();
        this.EventStartListening<GameCompleteEvent>();
    }

    private void OnDisable()
    {
        this.EventStopListening<GameStartEvent>();
        this.EventStopListening<GameCompleteEvent>();
    }

    public void OnGameStartButtonClicked()
    {
        GameStartEvent.Trigger();
    }

    public void OnEvent(GameStartEvent eventType)
    {
        m_GameStartPanel.SetActive(false);
        m_InGamePanel.SetActive(true);
        if (m_InputEnabled)
        {
            m_InputPanel.SetActive(true);
        }

        m_TimerCoroutine = StartCoroutine(TimerCoroutine());
    }

    private IEnumerator TimerCoroutine()
    {
        var l_Delay = new WaitForSeconds(1f);
        while (true)
        {
            m_Timer++;
            if (m_Timer == Constant.MOVING_ENEMY_GENERATION_START_TIME)
            {
                GenerateMovingEnemyEvent.Trigger();
            }
            yield return l_Delay;
        }
    }

    public void OnEvent(GameCompleteEvent eventType)
    {
        Time.timeScale = 0;

        if (m_TimerCoroutine != null)
        {
            StopCoroutine(m_TimerCoroutine);
            m_TimerCoroutine = null;
        }

        // set time and score
        int l_Minute = m_Timer / 60;
        int l_Seconds = m_Timer % 60;
        m_GameCompletePanel.Timer = string.Format(Constant.SURVIVED_TIME_MESSAGE, l_Minute.ToString().PadLeft(2, '0') + ":" + l_Seconds.ToString().PadLeft(2, '0'));
        m_GameCompletePanel.Score = string.Format(Constant.SCORE_MESSAGE, ScoreManager.Instance.Score);
        m_GameCompletePanel.gameObject.SetActive(true);

        m_InGamePanel.SetActive(false);
        if (m_InputEnabled)
        {
            m_InputPanel.SetActive(false);
        }
    }

    public void OnTryAgainButtonClicked()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void OnExitButtonClicked()
    {
        Application.Quit();
    }
}
