﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour, EventListener<GameStartEvent>,
                                               EventListener<GameCompleteEvent>
{
    private const float PLAYER_BOTTOM_OFFSET = 0.5f;

    [SerializeField]
    private GameManager m_GameManager = default;

    [SerializeField]
    private LayerMask m_RaycastMask = default;

    private List<Vector2> m_LanesPositionsForPlayer;

    private int m_CurrentLane = 0;

    private RaycastHit2D m_RaycastHit2D;

    private Coroutine m_AILogicCoroutine;

    [SerializeField]
    private bool m_IsAIEnabled = false;

    private void Start()
    {
        SetupLanePositions();

        SetPlayerInRandomLane();
    }

    private void OnEnable()
    {
        this.EventStartListening<GameStartEvent>();
        this.EventStartListening<GameCompleteEvent>();
    }

    private void OnDisable()
    {
        this.EventStopListening<GameStartEvent>();
        this.EventStopListening<GameCompleteEvent>();
    }

    private void SetPlayerInRandomLane()
    {
        m_CurrentLane = UnityEngine.Random.Range(0, m_LanesPositionsForPlayer.Count);
        GoToLane(m_CurrentLane);
    }

    private void SetupLanePositions()
    {
        int l_Lanes = m_GameManager.LanesCount;
        m_LanesPositionsForPlayer = new List<Vector2>(l_Lanes);

        float l_YPosition = (Camera.main.orthographicSize - GetComponent<SpriteRenderer>().bounds.size.y) - PLAYER_BOTTOM_OFFSET;
        float l_Offset = Screen.width / l_Lanes;

        float l_InitialPosition = l_Offset / 2f;

        Vector3 l_Position;
        for (int i = 0; i < l_Lanes; i++)
        {
            l_Position = Camera.main.ScreenToWorldPoint(new Vector2(l_InitialPosition + (l_Offset * i), 0));
            l_Position.y = -l_YPosition;
            m_LanesPositionsForPlayer.Add(l_Position);
        }
    }

    private bool GoToLane(int laneIndex)
    {
        if (laneIndex < 0 || laneIndex >= m_LanesPositionsForPlayer.Count)
        {
            return false;
        }

        transform.position = m_LanesPositionsForPlayer[laneIndex];
        return true;
    }

    public void MoveLeft()
    {
        m_CurrentLane--;
        if (!GoToLane(m_CurrentLane))
        {
            m_CurrentLane++;
        }
    }

    public void MoveRight()
    {
        m_CurrentLane++;
        if (!GoToLane(m_CurrentLane))
        {
            m_CurrentLane--;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(Constant.ENEMY_BODY_TAG))
        {
            GameCompleteEvent.Trigger();
        }
    }

    public void OnEvent(GameStartEvent eventType)
    {
        if (m_IsAIEnabled)
        {
            m_AILogicCoroutine = StartCoroutine(AILogic());
        }
    }

    private IEnumerator AILogic()
    {
        var l_Delay = new WaitForEndOfFrame();
        var l_DelayForRaycast = new WaitForSeconds(0.6f);
        while (true)
        {
            // AI
            Debug.DrawRay(transform.position, transform.up * 2.5f, Color.white);
            m_RaycastHit2D = Physics2D.Raycast(transform.position, transform.up, 2.5f, m_RaycastMask);

            if (m_RaycastHit2D.collider != null)
            {
                // if player at end of the lane like 0 or last lane then move to besides lane
                if (m_CurrentLane == 0)
                {
                    MoveRight();
                    yield return l_DelayForRaycast;
                }
                else if (m_CurrentLane == m_LanesPositionsForPlayer.Count - 1)
                {
                    MoveLeft();
                    yield return l_DelayForRaycast;
                }
                else
                {
                    var l_EnemyController = m_RaycastHit2D.collider.GetComponent<EnemyController>();

                    // clockwise movement
                    if (l_EnemyController.TargetLane > l_EnemyController.CurrentLane)
                    {
                        if (l_EnemyController.CurrentLane < m_CurrentLane)
                        {
                            MoveRight();
                        }
                        else if (l_EnemyController.CurrentLane == m_CurrentLane)
                        {
                            MoveLeft();
                        }

                        yield return l_DelayForRaycast;

                    }
                    // anticlockwise movement
                    else if (l_EnemyController.TargetLane < l_EnemyController.CurrentLane)
                    {
                        if (l_EnemyController.CurrentLane > m_CurrentLane)
                        {
                            MoveLeft();
                        }
                        else if (l_EnemyController.CurrentLane == m_CurrentLane)
                        {
                            MoveRight();
                        }
                        yield return l_DelayForRaycast;
                    }
                    // no movement
                    else
                    {
                        // move the player to any other lane
                        // here what we can do it raycast lanes those beside current lane
                        // find rayCastHit object and check the distance 
                        // which has large distance, we can move to that lane
                        if (Constant.GetRandomNumber() == 0)
                        {
                            MoveLeft();
                        }
                        else
                        {
                            MoveRight();
                        }
                    }
                }
            }
            yield return l_Delay;
        }
    }

    public void OnEvent(GameCompleteEvent eventType)
    {
        if (m_AILogicCoroutine != null)
        {
            StopCoroutine(m_AILogicCoroutine);
            m_AILogicCoroutine = null;
        }
    }
}
