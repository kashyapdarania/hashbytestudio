﻿using System.Collections;
using UnityEngine;

public class HorizontalMovingBehaviour : MonoBehaviour
{
    private const float HORIZONTAL_SPEED_MIN = 1;
    private const float HORIZONTAL_SPEED_MAX = 2;

    [SerializeField]
    private Vector2 m_Point1 = default, m_Point2 = default;
    [SerializeField]
    private bool m_IsLoopable = true;

    [SerializeField]
    private float m_HorizontalSpeed = 0;

    private Coroutine m_MovementCoroutine;

    public void Initilize(Vector2 point1, Vector2 point2, bool loopable = true)
    {
        m_Point1 = point1;
        m_Point2 = point2;
        m_IsLoopable = loopable;
    }

    private void OnEnable()
    {
        m_HorizontalSpeed = UnityEngine.Random.Range(HORIZONTAL_SPEED_MIN, HORIZONTAL_SPEED_MAX);
        if (m_MovementCoroutine != null)
        {
            StopCoroutine(m_MovementCoroutine);
        }
        m_MovementCoroutine = StartCoroutine(MovementCoroutine());
    }

    private void OnDisable()
    {
        if (m_MovementCoroutine != null)
        {
            StopCoroutine(m_MovementCoroutine);
            m_MovementCoroutine = null;
        }

        this.enabled = false;
    }

    private IEnumerator MovementCoroutine()
    {
        float l_Timer;
        var l_Delay = new WaitForEndOfFrame();

        do
        {
            l_Timer = 0;
            // move p1 to p2
            do
            {
                transform.localPosition = Vector3.Lerp(m_Point1, m_Point2, l_Timer);
                l_Timer += Time.deltaTime * m_HorizontalSpeed;
                yield return l_Delay;
            } while (l_Timer <= 1f);

            // move p2 to p1
            l_Timer = 0;
            do
            {
                transform.localPosition = Vector3.Lerp(m_Point2, m_Point1, l_Timer);
                l_Timer += Time.deltaTime * m_HorizontalSpeed;
                yield return l_Delay;
            } while (l_Timer <= 1f);
        } while (m_IsLoopable);
    }
}
