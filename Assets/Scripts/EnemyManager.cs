﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour, EventListener<GameStartEvent>,
                                           EventListener<GameCompleteEvent>,
                                           EventListener<GenerateMovingEnemyEvent>
{
    private const float TOP_OFFSET = 0.5f;

    private const int NORMAL_ENEMY_SCORE = 1;
    private const int MOVING_ENEMY_SCORE = 5;

    [SerializeField]
    private EnemyController m_EnemyPrefab = default;
    [SerializeField]
    private GameManager m_GameManager = default;

    [Range(2f, 4f)]
    [SerializeField]
    private float m_EnemyGenerationTime = default;

    private List<Vector2> m_LanesPositionsForEnemy;

    private List<EnemyController> m_Enemies;

    private Coroutine m_EnemyCoroutine;

    private WaitForSeconds m_EnemyGenerationDelay;
    private bool m_CanGenerateMovingEnemy = false;

    private float m_LaneDifference;

    private int m_MaxEnemiesAtStart = 8;

    private void Awake()
    {
        m_Enemies = new List<EnemyController>(m_MaxEnemiesAtStart);
        m_EnemyGenerationDelay = new WaitForSeconds(m_EnemyGenerationTime);
        CreateEnemies();
    }

    private void CreateEnemies()
    {
        EnemyController l_Enemy;
        for (int i = 0; i < m_MaxEnemiesAtStart; i++)
        {
            l_Enemy = Instantiate(m_EnemyPrefab);
            l_Enemy.gameObject.SetActive(false);
            m_Enemies.Add(l_Enemy);
        }
    }

    private void OnEnable()
    {
        this.EventStartListening<GameStartEvent>();
        this.EventStartListening<GameCompleteEvent>();
        this.EventStartListening<GenerateMovingEnemyEvent>();
    }

    private void Start()
    {
        SetupLanePositions();
    }

    private void OnDisable()
    {
        this.EventStopListening<GameStartEvent>();
        this.EventStopListening<GameCompleteEvent>();
        this.EventStopListening<GenerateMovingEnemyEvent>();
    }

    private void SetupLanePositions()
    {
        int l_Lanes = m_GameManager.LanesCount;
        m_LanesPositionsForEnemy = new List<Vector2>(l_Lanes);

        float l_YPosition = (Camera.main.orthographicSize + m_EnemyPrefab.Height) + TOP_OFFSET;
        float l_Offset = Screen.width / l_Lanes;

        float l_InitialPosition = l_Offset / 2f;

        Vector3 l_Position;
        for (int i = 0; i < l_Lanes; i++)
        {
            l_Position = Camera.main.ScreenToWorldPoint(new Vector2(l_InitialPosition + (l_Offset * i), 0));
            l_Position.y = l_YPosition;
            m_LanesPositionsForEnemy.Add(l_Position);
        }

        m_LaneDifference = Vector3.Distance(m_LanesPositionsForEnemy[0], m_LanesPositionsForEnemy[1]);
    }

    public void OnEvent(GameStartEvent eventType)
    {
        m_EnemyCoroutine = StartCoroutine(GenerateEnemy());
    }

    private int GetRandomLaneBesided(int laneIndex)
    {
        if (laneIndex <= 0)
        {
            return 1;
        }

        if (laneIndex >= m_LanesPositionsForEnemy.Count - 1)
        {
            return laneIndex - 1;
        }

        if (Constant.GetRandomNumber() == 0)
        {
            return laneIndex + 1;
        }
        else
        {
            return laneIndex - 1;
        }
    }

    private IEnumerator GenerateEnemy()
    {
        EnemyController l_Enemy;
        int l_RandomLane;
        while (true)
        {
            l_Enemy = GetEnemy();
            l_RandomLane = GetRandomLane();

            if (m_CanGenerateMovingEnemy)
            {
                l_Enemy.MoveTowards(l_RandomLane, GetRandomLaneBesided(l_RandomLane), m_LaneDifference);
                l_Enemy.ScoreValue = MOVING_ENEMY_SCORE;
            }
            else
            {
                l_Enemy.ScoreValue = NORMAL_ENEMY_SCORE;
            }

            GoToLane(l_Enemy, l_RandomLane);
            yield return m_EnemyGenerationDelay;
        }
    }

    private EnemyController GetEnemy()
    {
        for (int i = 0; i < m_Enemies.Count; i++)
        {
            if (!m_Enemies[i].gameObject.activeInHierarchy)
            {
                return m_Enemies[i];
            }
        }

        var l_Enemy = Instantiate(m_EnemyPrefab);
        m_Enemies.Add(l_Enemy);
        return l_Enemy;
    }

    private int GetRandomLane()
    {
        return UnityEngine.Random.Range(0, m_LanesPositionsForEnemy.Count);
    }

    private bool GoToLane(EnemyController enemyController, int laneIndex)
    {
        if (laneIndex < 0 || laneIndex >= m_LanesPositionsForEnemy.Count)
        {
            return false;
        }

        enemyController.transform.position = m_LanesPositionsForEnemy[laneIndex];
        enemyController.gameObject.SetActive(true);
        return true;
    }

    public void OnEvent(GameCompleteEvent eventType)
    {
        if (m_EnemyCoroutine != null)
        {
            StopCoroutine(m_EnemyCoroutine);
            m_EnemyCoroutine = null;
        }
    }

    public void OnEvent(GenerateMovingEnemyEvent eventType)
    {
        m_CanGenerateMovingEnemy = true;
    }
}
