﻿using UnityEngine;
using UnityEngine.UI;

public class GameCompletePanel : MonoBehaviour
{
    [SerializeField]
    private Text m_ScoreText = default;

    [SerializeField]
    private Text m_TimerText = default;

    public string Score
    {
        set
        {
            m_ScoreText.text = value;
        }
    }

    public string Timer
    {
        set
        {
            m_TimerText.text = value;
        }
    }
}
